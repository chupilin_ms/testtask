import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'jquery/dist/jquery.slim.min';
import 'bootstrap/dist/js/bootstrap.bundle.min'
import 'popper.js';

import Main from './components/main/main';


import { Provider } from 'react-redux';
import configureStore from './redux/configureStore';

const store = configureStore({});

ReactDOM.render(
    <Provider store={store}>
        <HashRouter>
            <Main/>
        </HashRouter>
    </Provider>,
    document.getElementById('root')
);

