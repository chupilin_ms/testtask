import {EventEmitter} from 'events';
EventEmitter.prototype._maxListeners = 200;
let dragEvents = new EventEmitter();

export {dragEvents};
