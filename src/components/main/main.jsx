import React, {  } from 'react';
import './main.css';
import Tables from './components/tables/Tables'
import Data from '../../data'

const Main = (props) => {

    const closeModal = () => {
        console.log('close');
    };

    return (
        <div className="task">
            <div className="container">
                <div className="row">
                    <div className="col-md-11 task_title">Configure Data Fields</div>
                    <div className="col-md-1 task_close"
                         onClick={closeModal}></div>
                    <Tables
                        data={Data}/>
                </div>
            </div>
        </div>
    );
};

export default Main;
