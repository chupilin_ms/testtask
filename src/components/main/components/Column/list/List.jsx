import React from 'react';
import './List.css';
import { Draggable } from 'react-beautiful-dnd';
import {dragEvents} from '../../../../events';

const List = (props) => {

    const isDisabled = ( index) => {
        dragEvents.emit('changeNumberDisableItem', index);
    };

    return (
        <Draggable draggableId={props.item.id} index={props.index}
                   isDragDisabled={(props.columnId === 'column-2')
                   && props.index < props.numberDisableItem}>

            {(provided)=>(
                (props.columnId === 'column-2')?
                    <div className={!(props.index < props.numberDisableItem)?'drag_list_visible':'drag_list_block'}
                                   {...provided.draggableProps}
                                   {...provided.dragHandleProps}
                                    ref={provided.innerRef}
                                    onDoubleClick={()=>{
                                        props.index < props.numberDisableItem?
                                        isDisabled( props.index):
                                        isDisabled( props.index + 1)
                                    }}>
                        {props.item.name}
                </div>
                :

                <div className='drag_list'
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                              ref={provided.innerRef}>
                    {props.item.name}
                 </div>)}

        </Draggable>
    );
};

export default List;
