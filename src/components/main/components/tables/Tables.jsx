import React, { useState, useEffect } from 'react';
import './Tables.css';
import Column from '../Column/Column';
import { DragDropContext } from 'react-beautiful-dnd';
import {dragEvents} from '../../../events';

const Tables = (props) => {

    let [initialData, setInitialData] = useState(props.data);

    useEffect(() => {
        dragEvents.addListener('changeNumberDisableItem', changeNumber);
    });

    const changeNumber = (index) => {
        setInitialData({
            ...initialData,
            numberDisableItem: index
        })
    };

    const onDragEnd = (result) => {
        const {destination, source, draggableId} = result;

        if(!destination) {
            return;
        }
        if(destination.droppableId === source.droppableId
            && destination.index === source.index) {
            return;
        }

        const start = initialData.columns[source.droppableId];
        const finish = initialData.columns[destination.droppableId];

        if (start === finish) {
            const newItemId = [...start.itemIds];
            newItemId.splice(source.index, 1);
            newItemId.splice(destination.index, 0, draggableId);

            const newColumn = {
                ...start,
                itemIds: newItemId
            };

            const newState = {
                ...initialData,
                columns: {
                    ...initialData.columns,
                    [newColumn.id]: newColumn
                }
            };

            setInitialData(newState);
        }

        if (start !== finish) {
            const startNewItemId = [...start.itemIds];
            startNewItemId.splice(source.index, 1);
            const newStart = {
                ...start,
                itemIds: startNewItemId
            };

            const finishNewItemId = [...finish.itemIds];
            finishNewItemId.splice(destination.index, 0, draggableId);
            const newFinish = {
                ...finish,
                itemIds: finishNewItemId
            };

            const newState = {
                ...initialData,
                columns: {
                    ...initialData.columns,
                    [newStart.id]: newStart,
                    [newFinish.id]: newFinish
                }
            };

            setInitialData(newState);
        }
    };

    const getAvailable = () => {
        const finalObj = {
            availableArr: initialData.columns['column-1'].itemIds,
            visibleArr: initialData.columns['column-2'].itemIds,
            numberOfFixColumn: initialData.numberDisableItem
        };
        console.log(finalObj);
        return finalObj;
    };

    return (
        <DragDropContext onDragEnd={onDragEnd}>
            {initialData.columnOrder.map(columnId=>{
                const column = initialData.columns[columnId];
                const items = column.itemIds.map(id=>{
                    return initialData.data[id];
                });
                return  <div key={column.id} className="col-md-6">
                            <Column key={column.id} column={column}
                                    items={items} columnId={column.id}
                                    numberDisableItem={initialData.numberDisableItem}/>
                        </div>
            })}
            <button type="button" className="btn btn-secondary btn_save"
                    onClick={getAvailable}>save</button>
            <button type="button" className="btn btn-secondary btn_close">cancel</button>
        </DragDropContext>
    );
};

export default Tables;
