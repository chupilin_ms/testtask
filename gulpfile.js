const fs = require('fs');
const gutil = require('gulp-util');
const browserify = require('browserify');
const browserifyCss = require('browserify-css');
const babelify = require('babelify');
const source = require('vinyl-source-stream');
const gulp = require('gulp');
const {series} = require('gulp');
const nodemon = require('gulp-nodemon');
const inject = require('gulp-inject');

function build() {
    return browserify({
        entries: './src/index.jsx',
        extensions: ['.jsx'],
        debug: true
    })
        .transform(babelify, {
            presets: ['es2015', 'react'],
            plugins: ['transform-class-properties', 'transform-object-rest-spread']
        })
        .transform(browserifyCss, {
            global: true,
            rootDir: 'src',
            onFlush: function (options, done) {
                fs.appendFileSync('build/bundle.css', options.data);
                done(null);
            }
        })
        .bundle()
        .on('error', function (err) {
            gutil.log(gutil.colors.red.bold('[browserify error]'));
            gutil.log(err);
            this.emit('end');
        })
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('build'));
}

function svgCopy() {
    return gulp.src('src/img/*.svg')
        .pipe(gulp.dest('build/img'));
}

function htmlCopy() {
    return gulp.src('public/*.html')
        .pipe(gulp.dest('build'));
}

function serve() {
    nodemon({
        script: 'index.js',
        ext: 'js',
        env: {
            NODE_ENV: 'dev',
            PORT: 3000
        },
        ignore: ['./node_modules/**']
    });
}


function injectDepend() {

    var target = gulp.src('./build/index.html');
    var sources = gulp.src(['build/*.js', 'build/*.css'], {read: false});

    return target.pipe(inject(sources, {relative: true}))
        .pipe(gulp.dest('build'));
}

gulp.task('default', series(svgCopy, build, htmlCopy, injectDepend, serve));

